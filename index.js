/*
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
*/

/* [Exponent Operator and Template Literals]
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

//insert code here...
let num=2;
const getCube = num**3;
console.log(`The cube of ${num} is ${getCube}`);

/* [Array Destructuring and Template Literals]
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

//insert code here...
const address = ['258 Washington Ave NW', 'California', '90011'];

const [street, city, postal] = address;

console.log(`I live at ${street}, ${city} ${postal}`)


/* [Object Destructuring and Template Literals]
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

const animal = {
	name: 'Lolong',
	animalkind: 'saltwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
};

const {name, animalkind, weight, length} = animal;

console.log(`${name} was a ${animalkind}. He weighed at ${weight} with a measurement of ${length}.`);

/* [forEach() method and Implicit Return(arrow function => )]
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((numbers) =>
	console.log(numbers));


/* [reduce() method and Implicit Return(arrow function => )]
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers from array of numbers of instuction #9.
*/

let totalArray = numbers.reduce((acc, cur) =>
    acc + cur
);

console.log(totalArray);


/*  [Class-Based Object Blueprint / JavaScript Classes]
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class Dog{
	constructor(name, age, breed){
		this.name=name;
		this.age=age;
		this.breed=breed;
	}
}


const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = 'Miniature Dachshund';
console.log(myDog);


// 14. Create a git repository named S24.




